package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testInputPlanet() throws MarsRoverException {
		int planetX=3;
		int planetY=2;
		List<String> planetObstacles=new ArrayList<String>();
		
		planetObstacles.add("(0,1)");
		planetObstacles.add("(0,2)");

		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
	}
	
	@Test
	public void planetShouldContainObstacles() throws MarsRoverException {
		
		int planetX=3;
		int planetY=2;
		List<String> planetObstacles=new ArrayList<String>();
		
		planetObstacles.add("(0,1)");

		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
		assertTrue(inputPlanet.planetContainsObstacleAt(0, 1));
		
	}
	
	@Test
	public void planetShouldNotContainObstacles() throws MarsRoverException {
		
		int planetX=3;
		int planetY=2;
		List<String> planetObstacles=new ArrayList<String>();
		
		planetObstacles.add("(0,1)");

		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
		assertFalse(inputPlanet.planetContainsObstacleAt(0, 2));
		
	}
	
	@Test 
	public void testNullExecutionCommand() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
		assertEquals("(0,0,N)",inputPlanet.executeCommand(""));

		
		
	}
	

	
	@Test 
	public void testTurningLeftLeftExecutionCommand() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
		assertEquals("(0,0,W)",inputPlanet.executeCommand("l"));

		
		
	}
	
	@Test 
	public void testTurningRightExecutionCommand() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
		assertEquals("(0,0,E)",inputPlanet.executeCommand("r"));

		
		
	}
	
	@Test 
	public void testMovingForwardExecutionCommand() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
		assertEquals("(0,1,N)",inputPlanet.executeCommand("f"));

		
		
	}
	@Test 
	public void testMovingBackwardExecutionCommand() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		inputPlanet.executeCommand("ff");
		
		assertEquals("(0,1,N)",inputPlanet.executeCommand("b"));

		
		
	}
	
	@Test 
	public void testMovingCombinedExecutionCommand() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
	
		assertEquals("(2,2,E)",inputPlanet.executeCommand("ffrff"));

		
		
	}
	
	@Test 
	public void planetShouldBeASphereWithMovingBackwards() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		
	
		assertEquals("(0,9,N)",inputPlanet.executeCommand("b"));

		
		
	}
	
	@Test 
	public void planetShouldBeASphereWithMovingForwards() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
		inputPlanet.setX(0);
		inputPlanet.setY(9);
	
		assertEquals("(0,0,N)",inputPlanet.executeCommand("f"));

		
		
	}
	
	@Test 
	public void planetShouldHaveOneObstacle() throws MarsRoverException {
		
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles=new ArrayList<String>();
		planetObstacles.add("(2,2)");
		
		MarsRover inputPlanet=new MarsRover(planetX,planetY,planetObstacles);
	
		assertEquals("(1,2,E)(2,2)",inputPlanet.executeCommand("frfff"));

		
		
	}
	
	

	
	
	
	
	
	

}
