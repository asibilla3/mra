package tdd.training.mra;

import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	private int roverPositionX;
	private int roverPositionY;
	private char roverDirection;
	private List<String> planetObstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		if(planetY<0||planetX<0) {
			throw new MarsRoverException("input negativi non validi");
		}else {
			this.planetY=planetY;
			this.planetX=planetX;
			this.planetObstacles=planetObstacles;
			this.roverPositionX=0;
			this.roverPositionY=0;
			 this.roverDirection='N';
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		// To be implemented
		String obstacle= (String)"("+x+","+y+")";
		
		if(this.planetObstacles.contains(obstacle)) {
			return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		// To be implemented
		char oneCommand;
		if(commandString==null) {


			return("(0,0,N)");
		}else {

			
			for(int i=0;i<commandString.length();i++) {
				if(commandString.charAt(i)=='l') {
					switch(this.roverDirection) {
					case 'N':
						System.out.print("1");
						this.setRoverDirection('W');
						break;
					case 'S':
						this.setRoverDirection('E');
						System.out.print("3");

						break;

					case 'W':
						this.setRoverDirection('S');
						System.out.print("4");
						break;
					case'E':
						this.setRoverDirection('N');
						System.out.print("5");


						break;
					}

				}

				if(commandString.charAt(i)=='r') {
					switch(this.roverDirection) {
					case 'N':
						this.setRoverDirection('E');
						break;
					case 'S':
						this.setRoverDirection('W');

						break;

					case 'W':
						this.setRoverDirection('N');

						break;
					case'E':
						this.setRoverDirection('S');

						break;
					}

				}



				if(commandString.charAt(i)=='f') {
					
					switch(this.roverDirection) {
					
					case 'N':
						
						if(this.getY()==planetY-1 && !(this.planetContainsObstacleAt(0,getX()))) {

							this.setY(0);
							
						}else if(!(this.planetContainsObstacleAt(getY()+1,getX()))) {
							
							
							this.setY(this.getY()+1);
							
						}else{
							
							

						}

						break;
					case 'S':
						this.setY(this.getY()-1);

						break;

					case 'W':
						
						
						this.setX(this.getX()-1);

						break;
					case'E':
						
						if(this.getX()>=planetX-1) {

							this.setX(0);
						}else {

							this.setX(this.getX()+1);
						}

						break;
					}
				}
				
				if(commandString.charAt(i)=='b') {
					
					switch(this.roverDirection) {
					
					case 'N':
						if(this.getY()<=0) {
							
							this.setY(this.planetY-1);
						}else {
							
						this.setY(this.getY()-1);
						}
						break;
					case 'S':
						this.setY(this.getY()+1);

						break;

					case 'W':
						this.setX(this.getX()+1);

						break;
					case'E':
						
						if(this.getX()==0) {

							this.setX(this.planetX-1);
						}else {

							this.setX(this.getX()-1);
						}

						break;
					}
				}


			}

			return "("+roverPositionX+","+roverPositionY+","+roverDirection+")";

		}
	}
	
	public int getX() {
		return this.roverPositionX;
	}
	
	public int getY() {
		
		return this.roverPositionY;
		
	}
	
	public void setX(int newX) {
		this.roverPositionX=newX;
		
	}
	
	public void setY(int newY) {
		this.roverPositionY=newY;
		
	}
	
	public void setRoverDirection(char newRoverDirection) {
		
		this.roverDirection=newRoverDirection;
		
	}
	
	
	
	
	

}
